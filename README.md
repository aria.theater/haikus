# Haikus

Haikus is a Python library for finding haikus in arbitrary text using NLTK.

It simply evaluates each line of a string, evaluating syllable count and matching on the 5/7/5 pattern.

This variant was forked from [codetojoy's fork](https://href.li/?https://github.com/codetojoy/haikus) of the [original project](https://href.li/?https://github.com/wieden-kennedy/haikus); both of us doing so to get library working again with the latest version of Python 🙃


## Installation

There's no PyPi package, unfortunately. You can install from source, though!

### pip

```sh
pip install -e git+https://gitlab.com/aria.theater/haikus
```

### poetry

Add this to your `pyproject.toml`:

```toml
haikus = { git = "https://gitlab.com/aria.theater/haikus" }
```

Update your project:

```sh
poetry update
```


## Usage

It's easy to find haikus in text with Haikus.

Simply instantiate a HaikuText object with your choice of text and ask it to find haikus.

```py
from haikus import HaikuText
text = HaikuText(
  text="""\
    My blahaj bounces
    round and round in the drier
    wide eyes asking why"""
)
haikus = text.get_haikus()
if haikus:
  haikus[0].get_lines()
```

`HaikuText.get_haikus()` will return a `Haiku` instance for every haiku in the text.

```py
[<haikus.haikutext.Haiku object at 0x7f4c1e70f220>]
```

The truthiness check will match on our input and `Haiku.get_lines()` will return a `list` containing lines found to be Haiku! ☺

```py
['my blahaj bounces', 'round and round in the drier', 'wide eyes asking why']
```


## Evaluation

`haikus` includes functions for evaluating `Haiku` objects based on their contents.  `haiku.calculate_quality()` will
uses a set of evaluators to determine quality. You can use the provided set of weighted evaluators or create your own.

```py
haiku.calculate_quality()
# returns quality according to the DEFAULT_HAIKU_EVALUATORS

MY_RAD_EVALUATORS = [
  # look at these evaluator, weight tuples!
  (IsAwesomeEvaluator, 1),
  (ContainsDogEvaluator, 0.75),
]
haiku.calculate_quality(evaluators=MY_RAD_EVALUATORS)
# this will calculate the quality accordin to MY_RAD_EVALUATORS
```

You can create your own evaluators by inheriting from ```haiku.evaluators.HaikuEvaluator``` and implementing the
`evaluate()` method. See `haiku.evaluators` for examples.

For more usage examples, see the tests, or check out [Django-Haikus](https://href.li/?https://github.com/wieden-kennedy/django-haikus/)

